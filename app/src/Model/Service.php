<?php

namespace harpya\discover\Model;

use harpya\discover\Constants;
use harpya\discover\Exceptions\ServiceException;
use Phalcon\Mvc\Model\Message;
use DenchikBY\MongoDB\Model;

class Service extends Model
{
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';


    protected $_pack;
    /**
     * @Primary
     * @Identity
     * @Column(type='integer', nullable=false)
     */
    public $id;

    /**
     * @Column(type='string', length=80, nullable=false)
     */
    public $key;

    /**
     * @Column(type='string', length=80, nullable=false)
     */
    public $name;

    /**
     * @Column(type='string', length=10, nullable=false)
     */
    public $version;

    /**
     * @Column(type='string', length=100, nullable=false)
     */
    public $host;

    /**
     * @Column(type='string', length=100, nullable=false)
     */
    public $health_check_url = '/ping';

    /**
     * @Column(type='int', nullable=false)
     */
    public $port;

    /**
     * @Column(type='string', length=30, nullable=true)
     */
    public $status = 'active';


    public $last_status_check = [];

    public static function getSource()
    {
        return 'service';
    }

    public function columnMap()
    {
        //Keys are the real names in the table and
        //the values their names in the application
        return [
            'id' => 'id',
            'key' => 'key',
            'status' => 'status',
            'name' => 'name',
            'version' => 'version',
            'host' => 'host',
            'port' => 'port',
            'health_check_url' => 'health_check_url',
            'created_at' => 'createdAt',
            'updated_at' => 'updatedAt',
            'last_status_check'=>'last_status_check'
        ];
    }

    /**
     *
     * TODO pull this method to a upper abstraction
     *
     * @param array $pack
     * @return Service
     */
    public static function make($pack) {
        $model = self::init($pack);
        $model->validate();
        return $model;
    }

    public static function init($pack=[]) {
        $obj = parent::init($pack);
        $obj->bind($pack);
        return $obj;
    }



    /**
     *
     * TODO pull this method to a upper abstraction
     *
     * @param array|null $attributes
     * @return mixed
     */
    public function save(array $attributes = null) {
        $this->validate();
        return parent::save();
    }


    /**
     * @throws ServiceException
     */
    public function validate()
    {
        if (!$this->name) {
            throw new ServiceException("Field 'name' is required, and is missing {$this->name}", Constants::EXCEPTION_SERVICE_INVALID_DATA);
        } elseif (strlen($this->name) < 2 || strlen($this->name) > 80) {
            throw new ServiceException("name must have between 2 and 80 characters", Constants::EXCEPTION_SERVICE_INVALID_DATA);
        }
    }

    /**
     * @param array $values
     * @return mixed
     */
    public function update($values = [])
    {
        $pack = array_merge($this->_pack ?? [], $values);
        $pack['updated_at'] = new \MongoDB\BSON\UTCDateTime();
        return parent::update($pack);
    }

    /**
     * @param array $values
     * @param bool $isStrict
     * @return array
     */
    public function bind($values = [], $isStrict=true)
    {
        $attrs = get_object_vars($this);

        $pack = [];
        foreach ($attrs as $k => $v) {
            if ($isStrict) {
                if (isset($values[$k])) {
                    $pack[$k] = $values[$k];
                    $this->$k = $values[$k];
                }
            } else {
                $pack[$k] = $values[$k];
                $this->$k = $values[$k];
            }

        }
        $this->_pack = $pack;
        return $pack;
    }
}
