<?php

namespace harpya\discover\Service;

use \Phalcon\Http\Request;
use \Phalcon\Http\Response;

class Security
{
    public function genKey(Request $request, Response &$response)
    {
//        $return = ['ok'=>1, 'path'=>getenv('KEY_PATH'), 'myuid'=>getmyuid(),'gid'=>getmygid()];
//
//        return $response->setContent(json_encode($return));

        $obj = new \harpya\discover\AuthToken();
        $obj->getKey();
        $obj->saveKey(getenv('KEY_PATH'));

        $return = ['ok' => 1, 'path' => getenv('KEY_PATH')];

        $response->setContent(json_encode($return));
    }
}
