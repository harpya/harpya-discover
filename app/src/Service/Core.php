<?php

namespace harpya\discover\Service;

use \Phalcon\Http\Request;
use \Phalcon\Http\Response;

/**
 * Class Core
 * @package harpya\discover\Service
 */
class Core
{
    /**
     * @param Request $request
     * @param Response $response
     */
    public function check(Request $request, Response &$response)
    {
        $resp = [];

        $resp['tm'] = time();
        $resp['method'] = $request->getMethod();
        $resp['hdr'] = $request->getHeaders();
        $resp['serverAddress'] = $request->getServerAddress();
        $resp['clientAddress'] = $request->getClientAddress();

        $response->setStatusCode(200, 'OK');
        $response->setHeader('Content-Type', 'text/json');
        $response->setContent(json_encode($resp));
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function routeNotFound(Request $request, Response &$response)
    {
        $response->setStatusCode(404, 'Not found');
        $response->setHeader('Content-Type', 'text/json');
        $response->setContent(json_encode(['success' => false, 'msg' => 'route not found', 'tm' => time()]));
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function phpInfo(Request $request, Response &$response)
    {
        $response->setStatusCode(200, 'OK');
        $response->setHeader('Content-Type', 'text/html');
        $contents = '';
        ob_start();
        phpinfo();
        $contents .= ob_get_clean();
        $response->setContent($contents);
    }

    public function ping(Request $request, Response &$response)
    {
        $response->setHeader('Content-Type', 'text/json');
        $response->setStatusCode(200, 'OK');
        $response->setContent(json_encode(['pong' => 1]));
    }

    public function testAuth(Request $request, Response &$response)
    {
        $response->setHeader('Content-Type', 'text/json');
        $response->setStatusCode(200, 'OK');
        $response->setContent(json_encode(['ok' => 1, 'tm' => time()]));
    }
}
