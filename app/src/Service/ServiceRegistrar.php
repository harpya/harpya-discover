<?php

namespace harpya\discover\Service;

use App\model\Model;
use GuzzleHttp\Client;
use harpya\discover\Application;
use harpya\discover\Constants;
use harpya\discover\Helpers\ServiceValidator;
use harpya\discover\Model\Service;
use harpya\discover\Validator;
use \Phalcon\Http\Request;
use \Phalcon\Http\Response;

/**
 * Class Service
 * @package harpya\discover\Service
 */
class ServiceRegistrar
{
    /**
     * @param Request $request
     * @param Response $response
     */
    public function registerService(Request $request, Response &$response)
    {

        try {
            $resp = [];
            $resp['tm'] = time();

            $service = $request->getPost('service');
            $serviceKey = $this->generateKey($service);

                $objService = $this->createModel($service, $serviceKey);



            $status = $objService->save();

            if ($status) {
                $resp['success'] = true;
                $resp['msg'] = "service '{$service['name']}' registered with success";
                $resp['address'] = $service['host'] . ':' . $service['port'];
                $resp['key'] = $serviceKey;
            } else {
                $resp['success'] = false;
                $resp['msg'] = 'Error in record creation';
            }
        } catch (\Exception $e) {
            $resp['success'] = false;

            Application::getInstance()->getLogHandler()->logException($e);

            if (strpos($e->getMessage(), 'E11000 duplicate key') === false) {
                $resp['msg'] = $e->getMessage();
                $resp['code'] = $e->getCode();
            } else {
                $resp['msg'] = "service '{$service['name']}' already registered on this host/port";
                $resp['code'] = Constants::EXCEPTION_SERVICE_ALREADY_REGISTERED;
            }
        }

        $response->setContent(json_encode($resp));
    }


    /**
     * Given the service name, and optionally the parameter version, try to find a
     * service provider, and return its key
     *
     * @param Request $request
     * @param Response $response
     * @param array $data
     */
    public function selectService(Request $request, Response &$response, $data = [])
    {
        $query = ['name' => $data['name'], 'status' => Service::STATUS_ACTIVE];

        if (isset($data['version'])) {
            $query['version'] = $data['version'];
        }

        $objService = Service::init();
        $services = $objService->find($query);

        if (empty($services) || $services->count() == 0) {
            $resp['success'] = false;
            $resp['msg'] = "Service '{$query['name']}'' not found";
            $response->setStatusCode(404, 'Error');
        } else {
            $resp['success'] = true;

            $rows = [];
            $items = json_decode($services, true);

            foreach ($items as $item) {
                $rows[] = [
                    'key' => $item['key'],
                    'version' => $item['version'],
                    'host' => $item['host'],
                    'port' => $item['port'],
                    'status' => $item['status'],
                ];
            }

            $resp['rows'] = $rows;
            $resp['count'] = count($resp['rows']);
        }

        $response->setContent(json_encode($resp, true));
    }

    /**
     *
     * Given the service provider key, returns the service provider record
     *
     * @param Request $request
     * @param Response $response
     * @param array $data
     */
    public function getService(Request $request, Response &$response, $data = [])
    {
        $key = $data['key'];

        $resp = [];
        $resp['key'] = $key;

        $builder = Service::query();
        $builder->where('key', '=', $key);

        $count = $builder->count();

        if ($count === 0) {
            $resp['success'] = false;
            $resp['msg'] = "Record with key $key was not found";
            $response->setStatusCode(404, 'Error');
        } else {
            $service = $builder->first()->toArray();
            $resp = [
                'name' => $service['name'],
                'version' => $service['version'],
                'key' => $service['key'],
                'host' => $service['host'],
                'port' => $service['port'],
            ];
        }

        $response->setContent(json_encode($resp));
        return $resp;
    }

    /**
     * Remove the informed service provider (by key) from local database
     *
     * @param Request $request
     * @param Response $response
     * @param array $data
     */
    public function unregisterService(Request $request, Response &$response, $data = [])
    {
        $key = $data['key'];

        $resp = [];
        $resp['key'] = $key;

        $builder = Service::query();
        $builder->where('key', '=', $key);

        $count = $builder->count();

        if ($count === 0) {
            $resp['success'] = false;
            $resp['msg'] = "Record with key $key was not found";
            $response->setStatusCode(404, 'Error');
        } else {
            try {
                $builder->delete();
                $resp['success'] = true;
            } catch (\Exception $e) {
                $resp['success'] = false;
                $resp['msg'] = $e->getMessage();
                $response->setStatusCode(400, 'Error');
            }
        }
        $response->setContent(json_encode($resp));
    }

    /**
     * Update some attribute from a given service (required pass 'key' parameter on array $data)
     *
     * @param Request $request
     * @param Response $response
     * @param array $data
     */
    public function updateService(Request $request, Response &$response, $data = [])
    {
        $resp = [];
        $key = $data['key'];
        $resp['key'] = $key;
        $resp['success'] = false;

        $attrs = $request->getPost('service');
        if (is_array($attrs) && !empty($attrs)) {
            $builder = Service::query();
            $builder->where('key', '=', $key);

            $count = $builder->count();

            if ($count == 0) {
                $resp['msg'] = "Record not found (key=$key)";
                $response->setStatusCode(404, 'Error');
            } else {
                $model = $builder->first();
                $resp['result'] = $toUpdate = $model->bind($attrs);
                if (empty($toUpdate)) {
                    $resp['msg'] = 'None attribute(s) to update';
                    $response->setStatusCode(400, 'Error');
                } else {
                    try {
                        $model->update();
                        $resp['success'] = true;
                        $response->setStatusCode(200, 'Ok');
                    } catch (\Exception $e) {
                        $resp['msg'] = $e->getMessage();
                        $response->setStatusCode(400, 'Error');


//                        echo $e->getMessage()."\n";

                        $resp['success'] = false;

                        Application::getInstance()->getLogHandler()->logException($e);

                        if (strpos($e->getMessage(), 'E11000 duplicate key') === false) {
                            $resp['msg'] = $e->getMessage();
                            $resp['code'] = $e->getCode();
                        } else {
                            $resp['msg'] = "service '{$model->name}' already registered on this host/port";
                            $resp['code'] = Constants::EXCEPTION_SERVICE_ALREADY_REGISTERED;
                        }

                    }
                }
            }
        } else {
            $resp['msg'] = 'None attribute(s) to update';
            $response->setStatusCode(400, 'Error');
        }

        $response->setContent(json_encode($resp));
    }


    /**
     * @param Request $request
     * @param Response $response
     */
    public function pingServices(Request $request, Response &$response) {

        // 1st: get the service list, with more than X minutes since last check
        $list = $this->getListServicesToPing();


        $auxResponse = [];
        foreach ($list as $service) {
            try {
                $key = $service['key'];
                // 2nd: for each item, make a call to check if still available or not
                $result = $this->pingService($service);

                // 3rd update the service status
                $this->updateServicePingStatus($service,$result);
                $auxResponse[$key] = $result;

            } catch (\Exception $e) {
                // if any problem, register it
                $this->updateServicePingStatus($service, ['msg'=>$e->getMessage(), 'code'=>$e->getCode()]);
                $auxResponse[$key] = ['msg'=>$e->getMessage(), 'code'=>$e->getCode()];
            }

        }

        $response->setStatusCode(200, 'Ok');
        $response->setContent(\GuzzleHttp\json_encode($auxResponse));

    }

    /**
     * Return a list of services with more than N minutes since last check
     */
    protected function getListServicesToPing() {
        $response = [];
        $time = time()-30;

        $builder = Service::query();
        $builder->where('last_status_check.when', '<', $time);

        $services = $builder->get();

        foreach ($services as $item) {
            $arr = $item->toArray();
            $key = $arr['key'];
            $url = $arr['host'].':'.$arr['port'].$arr['health_check_url'];
            $response[] = ['url'=>$url, 'key'=>$key];
        }

        return $response;
    }



    protected function pingService($service) {
        $response = [];

        try {
            // make REST call
            $url = $service['url'];
            $client = new Client();
            $result = $client->get($url);

            $json = $result->getBody()->getContents();
            $arr = json_decode($json, true);

            if (isset($arr['command']) && ($arr['command'] === 'pong')) {
                $response['success'] = true;
            } else {
                $response = [
                    'success' => false,
                    'msg' => "Invalid response from $url",
                    'code' => Constants::ERROR_PING_SERVICE_INVALID_RESPONSE
                ];
            }

        } catch (\Exception $ex) {
            $response = ['success'=>false, 'msg'=> $ex->getMessage(), 'code'=>$ex->getCode(), 'class'=>get_class($ex)];
        }

        return $response;
    }

    /**
     * Update the service status record
     *
     * @param $service
     * @param array $record
     */
    protected function updateServicePingStatus($service, $record=[]) {
        $record['when'] = time();

        $builder = Service::query();
        $builder->where('key', '=', $service['key']);
        $model = $builder->first();


        $obj = $model->toArray();
        $obj['last_status_check'] = $record;

        if ($record['success']) {
            $obj['status'] = Service::STATUS_ACTIVE;
        } else {
            $obj['status'] = Service::STATUS_INACTIVE;
        }

        $model->bind($obj);
        $model->update();
    }


    ////////////////////////////
    ///// Helpers functions
    ///////////


    /**
     *
     * @param $service
     * @return string
     */
    protected function generateKey($service) {
        return hash('SHA256', time() . json_encode($service));
    }


    /**
     * Build an associative array with service data, to be persisted on DB
     * @param array $service
     * @param string $serviceKey
     * @return array
     */
    protected function createModel($service, $serviceKey)
    {
        $pack = [
            'version' => $service['version'] ?? 1,
            'name' => $service['name']??null,
            'key' => $serviceKey,
            'host' => $service['host']??$_SERVER['REMOTE_ADDR'],
            'port' => $service['port']??80,
            'health_check_url' => $service['health_check_url'] ?? '/ping',
            'created_at' => time(),
            'status' => Service::STATUS_ACTIVE,
            'last_status_check' => [
                'when'=>0
            ]
        ];

        $objService = Service::make($pack);

        return $objService;
    }

}
