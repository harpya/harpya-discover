<?php

namespace harpya\discover\Http;

use \Phalcon\Events\Event;
use \Phalcon\Mvc\Micro;
use \Phalcon\Mvc\Micro\MiddlewareInterface;

class ApiAuthMiddleware extends \harpya\phalcon\middleware\ApiAuthMiddleware implements MiddlewareInterface
{
    public function triggerInvalidToken()
    {
        $ex = new \harpya\phalcon\Exception\RuntimeException('Invalid Auth token', 403);
        $ex->setForcedHttpCode(403);

        throw $ex;
    }
}
