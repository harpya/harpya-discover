<?php

/**
 * @param $key
 * @param null $default
 * @return null
 */
function env($key, $default = null)
{
    if (isset($_ENV[$key])) {
        return $_ENV[$key];
    }
    return $default;
}
