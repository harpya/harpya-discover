<?php

include_once( __DIR__.'/routes.php');

//$app->addRouteNotFound('\harpya\discover\service\Core::routeNotFound()');

$app->get('/auth', '\harpya\discover\Service\Core::testAuth()');
$app->get('/ping', '\harpya\discover\Service\Core::ping()')->setName('ping');
$app->post('/ping', '\harpya\discover\Service\Core::invalidMethod()')->setName('ping');


$app->get('/check', '\harpya\discover\Service\Core::check()')->setName('check-app');
$app->post('/check', '\harpya\discover\Service\Core::check()')->setName('check-app-post');

$app->addGet('/info', '\harpya\discover\Service\Core::phpInfo()')->setName('info');
$app->addGet('/', '\harpya\discover\Service\Core::routeNotFound()');


//$app->addWhitelistedRoute(['info', 'gen_key','ping']);
