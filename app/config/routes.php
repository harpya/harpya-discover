<?php

$app->addRouteNotFound('\harpya\discover\Service\Core::routeNotFound()');

$app->get('/auth', '\harpya\discover\Service\Core::testAuth()');
$app->get('/ping',     '\harpya\discover\Service\Core::ping()')->setName('ping');


$app->get('/check', '\harpya\discover\Service\Core::check()')->setName('check-app');
$app->post('/check', '\harpya\discover\Service\Core::check()')->setName('check-app-post');

//$app->addGet('/check_key', '\harpya\discover\Service\Security::genKey()')->setName('gen_key');
$app->addGet('/info', '\harpya\discover\Service\Core::phpInfo()')->setName('info');
$app->addGet('/', '\harpya\discover\Service\Core::routeNotFound()');

$app->addPost('/service', '\harpya\discover\Service\ServiceRegistrar::registerService()');
$app->addOption('/service/{name}', '\harpya\discover\Service\ServiceRegistrar::selectService()', ['name']);
$app->addOption('/service/{name}/{version}', '\harpya\discover\Service\ServiceRegistrar::selectService()', ['name', 'version']);
$app->addGet('/service/{key}', '\harpya\discover\Service\ServiceRegistrar::getService()', ['key']);
$app->addDelete('/service/{key}', '\harpya\discover\Service\ServiceRegistrar::unregisterService()', ['key']);
$app->addPut('/service/{key}', '\harpya\discover\Service\ServiceRegistrar::updateService()', ['key']);


$app->addGet('/gen_key', '\harpya\discover\Service\Security::genKey()')->setName('gen_key');

$app->addWhitelistedRoute(['info', 'gen_key','ping','check_services']);



$app->addOption('/v1/service/{name}', '\harpya\discover\Service\ServiceRegistrar::selectService()', ['name']);
$app->addOption('/v1/service/{name}/{version}', '\harpya\discover\Service\ServiceRegistrar::selectService()', ['name', 'version']);
$app->addPost('/v1/service', '\harpya\discover\Service\ServiceRegistrar::registerService()');
$app->addOption('/v1/service/{name}', '\harpya\discover\Service\ServiceRegistrar::selectService()', ['name']);
$app->addOption('/v1/service/{name}/{version}', '\harpya\discover\Service\ServiceRegistrar::selectService()', ['name', 'version']);
$app->addGet('/v1/service/{key}', '\harpya\discover\Service\ServiceRegistrar::getService()', ['key']);
$app->addDelete('/v1/service/{key}', '\harpya\discover\Service\ServiceRegistrar::unregisterService()', ['key']);
$app->addPut('/v1/service/{key}', '\harpya\discover\Service\ServiceRegistrar::updateService()', ['key']);
$app->addGet('/v1/check_services', '\harpya\discover\Service\ServiceRegistrar::pingServices()')->setName('check_services');
