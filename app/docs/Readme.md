# Harpya Discover

Service Discover designed for microservices based applications, 
created using PHP 7.2 with Phalcon framework.

## Purpose

- Manage and introduce different versions of services, allowing migrate and rollback microservices updates with 
minimum impact to end user.
- Decouple the directory of microservices from each configuration instance;
- Facilitate the configuration of an network of services;

## Instalation

After run the command ``docker-compose up -d``, you should run ``docker-compose exec app php scripts/init_db.ph`` 
to initialize the DB.


## To test


````
docker-compose exec app vendor/bin/phpunit tests/functional
````

## API

