# Requirements

- When a new service subscribe the Discover instance, this should make a ping call to confirm that this instance is who it say be.
- Create a cron task on discover, to make pings periodically to all subscribed services, and update the network status
- when receive new metadata from microservice, when the subscription process, store these metadata too.
- after N tries to reach a service, change its status to inactive
- on bootsrap, create a SSL key-pair, and when a new service join to network, this receives the public key as part of answer 
- when send the ping command, send also an encrypted message using the private key. The service can them validate the message and check if this is who ite say be
- if the microservice sends the public key, store it also.
  
