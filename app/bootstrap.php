<?php

require_once __DIR__ . '/config/init.php';
require_once __DIR__ . '/src/functions.php';

require_once __DIR__ . '/vendor/autoload.php';
include_once __DIR__ . '/load_env.php';

use Phalcon\Mvc\Model\MetaData\Memory as MemoryMetaData;
use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Phalcon\Mvc\Model\MetaData\Strategy\Annotations as StrategyAnnotations;

$loader = new Loader();

$loader->registerNamespaces(
    [
        'harpya\discover\Model' => __DIR__ . '/src/Model',
    ]
);

$loader->register();

$di = new FactoryDefault();

// Set up the database service
$di->set(
    'config',
    function () {
        $user = env('MONGO_INITDB_ROOT_USERNAME');
        $pass = env('MONGO_INITDB_ROOT_PASSWORD');
        $host = env('MONGO_INITDB_HOST'); // MONGO_INITDB_HOST
        $port = env('MONGO_INITDB_PORT', 27017);

        $dbName = env(CONFIG_ENV_DATABASE, \harpya\discover\Constants::DEFAULT_DBNAME);

        return new \Phalcon\Config([
            'mongodb' => [
                'host' => $host,
                'port' => $port,
                'database' => $dbName,
                'user' => $user,
                'password' => $pass
            ]
        ]);
    },
    true
);

$di->set('mongo', function () use ($di) {
    $config = $di->get('config')->mongodb;
    $manager = new \MongoDB\Driver\Manager('mongodb://' . $config->host . ':' . $config->port, ['username' => $config->user,  'password' => $config->password]);
    return $manager;
}, true);

$di['modelsMetadata'] = function () {
    // Instantiate a metadata adapter
    $metadata = new MemoryMetaData(
        [
            'lifetime' => env(CONFIG_MEMORY_METADATA_LIFETIME, 86400),
            'prefix' => env(CONFIG_MEMORY_METADATA_LIFETIME, 'my-prefix')
        ]
    );

    // Set a custom metadata database introspection
    $metadata->setStrategy(
        new StrategyAnnotations()
    );

    return $metadata;
};


$cfgPack = [];

$cfgPack['routes'] = __DIR__ . "/config/routes.php";

$cfgPack['di'] = $di;

$cfgPack['middleware'] = [];
$cfgPack['middleware']['before'] = [];
$cfgPack['middleware']['before'][] = '\harpya\discover\Http\ApiAuthMiddleware';
$cfgPack['middleware']['before'][] = '\harpya\phalcon\middleware\RequestMiddleware';

$cfgPack['log'] = [];

switch (env(CONFIG_ENV_LOGGER, false)) {
    case LOGGER_SENTRY:
        $cfgPack['log'][] = new \harpya\phalcon\Report\SentryReport(getenv('SENTRY_DSN'));
        break;
}

$cfgPack['auth'] = [];

$app = \harpya\phalcon\Application::getInstance($cfgPack);
