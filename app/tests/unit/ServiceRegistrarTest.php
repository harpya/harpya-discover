<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-05-05
 * Time: 10:28 PM
 */

namespace unit;

include_once __DIR__.'/HasRequest.php';

use harpya\discover\Service\ServiceRegistrar;
use Phalcon\Http\Response;
use harpya\discover\Service\Core;

class ServiceRegistrarTest extends \PHPUnit\Framework\TestCase
{
use HasRequest;

    public function testCheckServices() {

        $request = $this->getRequest([
            'REMOTE_ADDR' => '192.168.10.26',
            'REQUEST_METHOD' => 'GET',
            'SERVER_ADDR' => '192.168.0.1'
        ]);

        $response = new Response();

        $core = new class extends ServiceRegistrar {

            public $lsStatus=[];

            public function getListServicesToPing() {
                $list = [];
                $list[] = ['url' => 'http://192.168.50.100:9001/ping', 'key'=>1];
                $list[] = ['url' => 'http://192.168.50.150:9001/ping', 'key'=>2];
                $list[] = ['url' => 'http://192.168.50.200:9001/ping', 'key'=>3];
                return $list;
            }

            public function pingService($service) {
                $response = [];
                if ($service['key']==2) {
                    throw new \Exception("Error communicating to service 2 - timeout",500);
                } else {
                    $response['success'] = true;
                }


                return $response;
            }

            public function updateServicePingStatus($service, $status=[]) {
                $status['when'] = time();
                $this->lsStatus[$service['key']] = $status;
            }
        };

        $core->pingServices($request, $response);

        $this->assertEquals(200,$response->getStatusCode() );

        $this->assertTrue(is_array($core->lsStatus));

        $this->assertCount(3, $core->lsStatus);


        $this->assertArrayHasKey('success', $core->lsStatus[1]);
        $this->assertEquals(1, $core->lsStatus[1]['success']);
        $this->assertArrayHasKey('when', $core->lsStatus[1]);

        $this->assertArrayHasKey('msg', $core->lsStatus[2]);
        $this->assertArrayHasKey('code', $core->lsStatus[2]);
        $this->assertEquals(500, $core->lsStatus[2]['code']);
        $this->assertArrayHasKey('when', $core->lsStatus[2]);

        $this->assertArrayHasKey('success', $core->lsStatus[3]);
        $this->assertEquals(1, $core->lsStatus[3]['success']);
        $this->assertArrayHasKey('when', $core->lsStatus[3]);

    }


}
