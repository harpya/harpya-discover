<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-05-05
 * Time: 10:29 PM
 */

namespace unit;

use Phalcon\Http\Response;
use harpya\discover\Service\Core;

trait HasRequest {

    protected function getRequest($props = [], $parms = [])
    {
        $request = new class($props, $parms) extends \harpya\phalcon\Request {
            public $map = [
                'server' => [
                    'REMOTE_ADDR' => 'REMOTE_ADDR',
                    'REQUEST_METHOD' => 'REQUEST_METHOD',
                    'SERVER_ADDR' => 'SERVER_ADDR'
                ]
            ];

            public function __construct($props = [], $parms = [])
            {
                foreach ($this->map['server'] as $k => $v) {
                    if (isset($props[$k])) {
                        $_SERVER[$v] = $props[$k];
                    }
                }
                parent::__construct();
                $this->json = $parms;
            }
        };
        return $request;
    }

}
