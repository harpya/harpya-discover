<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-03-19
 * Time: 11:31 PM
 */

namespace unit;

include_once __DIR__.'/HasRequest.php';

use Phalcon\Http\Response;
use harpya\discover\Service\Core;

class CoreTest extends \PHPUnit\Framework\TestCase
{
    use HasRequest;


    public function testCheckOk()
    {
        $myInitialProps = [
            'SERVER_ADDR' => 'localhost',
            'REMOTE_ADDR' => '192.168.1.1',
            'REQUEST_METHOD' => 'post'
        ];

        $parms = [ ];

        $request = $this->getRequest($myInitialProps, $parms);
        $response = new Response();

        $core = new Core();
        $core->check($request, $response);

        $this->assertEquals(200,$response->getStatusCode() );
        $json = $response->getContent();
        $arr = json_decode($json,true);

        $this->assertTrue(is_array($arr));
        $this->assertArrayHasKey('method', $arr);
        $this->assertEquals($myInitialProps['SERVER_ADDR'], $arr['serverAddress']);

    }


    public function testRouteNotFound() {
        $myInitialProps = [
            'SERVER_ADDR' => 'localhost',
            'REMOTE_ADDR' => '192.168.1.1',
            'REQUEST_METHOD' => 'post'
        ];

        $parms = [ ];

        $request = $this->getRequest($myInitialProps, $parms);
        $response = new Response();

        $core = new Core();
        $core->routeNotFound($request, $response);

        $this->assertEquals(404,$response->getStatusCode() );
        $json = $response->getContent();
        $arr = json_decode($json,true);

        $this->assertTrue(is_array($arr));
        $this->assertArrayHasKey('success', $arr);
        $this->assertFalse($arr['success']);
        $this->assertArrayHasKey('msg', $arr);
        $this->assertEquals('route not found',$arr['msg']);

    }
//
//    public function testPHPInfo() {
//
//    }
//
    public function testPing() {
        $myInitialProps = [
            'SERVER_ADDR' => 'localhost',
            'REMOTE_ADDR' => '192.168.1.1',
            'REQUEST_METHOD' => 'post'
        ];

        $parms = [ ];

        $request = $this->getRequest($myInitialProps, $parms);
        $response = new Response();

        $core = new Core();
        $core->ping($request, $response);

        $this->assertEquals(200,$response->getStatusCode() );
        $json = $response->getContent();
        $arr = json_decode($json,true);

        $this->assertTrue(is_array($arr));
        $this->assertArrayHasKey('pong', $arr);
        $this->assertEquals(1,$arr['pong']);
    }
//
//    public function testAuth() {
//
//    }
}
