<?php

namespace functional;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use harpya\discover\Model\Service;
use MongoDB\Driver\Command;

/**
 * To test this scenarios, let's consider the follow microservices:
 *
 * - user : manage all user (profile, login)
 * - billing : all operations related to plans, billing and so on.
 * - organization : entity that hold users in different levels/groups, to organize the services
 *
 * Let's suppose that we have 2 versions of billing (one using the payment gateway A, and the second
 * one migrating to gateway B). The idea is that I can register the both on our network, and we can just
 * switch on Discover which one should be used by services already running. Is supposed that have no impact
 * on other services.
 *
 * The second scenario is that the service 'organization' will have a huge change, and will affect
 * the user microservice.
 *
 * Class ServiceRegisterTest
 * @package functional
 */
class ServiceRegisterTest extends \PHPUnit\Framework\TestCase
{
    protected $config = ['base_uri' => 'http://webserver'];

    const DB_SERVICE = 'discover.service';

    const SERVICE_NAME = 'myServiceTest2';

    protected function getConfig($config = [])
    {
        return array_replace($this->config, $config);
    }

    /**
     * @return Client
     */
    protected function getClient()
    {
        $client = new Client($this->getConfig([
            'headers' => [
                'X-AUTH-API' => getenv('HARPYA_AUTH_API'),
            ]
        ]));
        return $client;
    }

    protected function getJSONFromResponse($res)
    {
        $text = $res->getBody()->getContents();
        $json = json_decode($text, true);
        return $json;
    }

    /**
     * Force clears the Discover DB
     */
    protected function clearDB()
    {
        $user = getenv('MONGO_INITDB_ROOT_USERNAME');
        $pass = getenv('MONGO_INITDB_ROOT_PASSWORD');
        $host = $_ENV['MONGO_INITDB_HOST']; // MONGO_INITDB_HOST

        $mongoHost = 'mongodb://' . $host;

        $bulk = new \MongoDB\Driver\BulkWrite();
        $bulk->delete(['name' => self::SERVICE_NAME]);

        $manager = new \MongoDB\Driver\Manager($mongoHost, ['username' => $user,  'password' => $pass]);

        $manager->executeBulkWrite(self::DB_SERVICE, $bulk);
    }

    protected function createIndex($manager)
    {
        $manager->executeCommand('discover', new Command(
            ['createIndexes' => 'service',
                'indexes' => [[
                    'name' => 'myIndexName',
                    'key' => ['name' => 1, 'host' => 1, 'port' => 1],
                    'ns' => self::DB_SERVICE,
                    'unique' => true
                ]]]
        ));
    }

    /**
     * Connect to Discover and create a new service entry on directory.
     */
    public function testRegisterNewService()
    {
        $this->clearDB();

        $client = $this->getClient();

        $res = $client->post('/v1/service', [
            'json' => [
                'service' => [
                    'name' => self::SERVICE_NAME,
                    'host' => 'http://ms_user',
                    'port' => '18001'
                ]
            ]
        ]);

        $json = $this->getJSONFromResponse($res);

        $this->assertEquals(200, $res->getStatusCode());
        $this->assertArrayHasKey('success', $json);
        $this->assertArrayHasKey('tm', $json);
        $this->assertTrue($json['success']);
    }

    /**
     * Connect to Discover and try to create a new service, with same
     * parameters the last created
     *
     * @depends testRegisterNewService
     */
    public function testRegisterDuplicatedService()
    {
        $client = $this->getClient();

        $res = $client->post('/v1/service', [
            'json' => [
                'service' => [
                    'name' => self::SERVICE_NAME,
                    'host' => 'http://ms_user',
                    'port' => '18001'
                ]
            ]
        ]);

        $json = $this->getJSONFromResponse($res);

        $this->assertEquals(200, $res->getStatusCode());
        $this->assertArrayHasKey('success', $json);
        $this->assertArrayHasKey('tm', $json);
        $this->assertArrayHasKey('code', $json);
        $this->assertEquals(1002, $json['code']);
        $this->assertFalse($json['success']);
    }

    /**
     * Make a couple requests to Discover, simulating different nodes
     * registering different services.
     */
    public function testRegisterServiceOnDifferentNodes()
    {
        $client = $this->getClient();

        $res1 = $client->post('/v1/service', [
            'json' => [
                'service' => [
                    'name' => self::SERVICE_NAME,
                    'host' => 'http://ms_user_01',
                    'port' => '18001'
                ]
            ]
        ]);

        $this->assertEquals(200, $res1->getStatusCode());

        $res2 = $client->post('/v1/service', [
            'json' => [
                'service' => [
                    'name' => self::SERVICE_NAME,
                    'host' => 'http://ms_user_02',
                    'port' => '18001',
                    'version' => '1.2'
                ]
            ]
        ]);

        $this->assertEquals(200, $res2->getStatusCode());

        $json1 = $this->getJSONFromResponse($res1);
        $json2 = $this->getJSONFromResponse($res2);

        $this->assertArrayHasKey('success', $json1);
        $this->assertArrayHasKey('success', $json2);
    }

    /**
     * After have created a service, make a call to query this same service
     */
    public function testRequestService()
    {
        $client = $this->getClient();

        $res = $client->options('/v1/service/' . self::SERVICE_NAME);

        $this->assertEquals(200, $res->getStatusCode());
        $json = $this->getJSONFromResponse($res);
        $this->assertArrayHasKey('rows', $json);
        $this->assertCount(3, $json['rows']);

        $res2 = $client->options('/v1/service/' . self::SERVICE_NAME . '/1.2');

        $this->assertEquals(200, $res2->getStatusCode());
        $json2 = $this->getJSONFromResponse($res2);
        $this->assertArrayHasKey('rows', $json2);
        $this->assertCount(1, $json2['rows']);
    }

    /**
     * Request to Discover a service that does not exists
     */
    public function testSelectInexistentService()
    {
        $client = $this->getClient();

        $this->expectException('GuzzleHttp\Exception\ClientException');
        $client->options('/v1/service/billing');
    }

    public function testGetService()
    {
        $client = $this->getClient();

        $res = $client->options('/v1/service/' . self::SERVICE_NAME);

        $this->assertEquals(200, $res->getStatusCode());
        $json = $this->getJSONFromResponse($res);

        $this->assertArrayHasKey('rows', $json);
        $this->assertCount(3, $json['rows']);

        $record = reset($json['rows']);

        $this->assertArrayHasKey('key', $record);

        $key = $record['key'];

        $res2 = $client->get("/v1/service/$key");
        $json2 = $this->getJSONFromResponse($res2);

        $this->assertTrue(is_array($json2));
        $this->assertArrayHasKey('key', $json2);
        $this->assertArrayHasKey('port', $json2);
        $this->assertEquals($record['key'], $json2['key']);
        $this->assertEquals('18001', $json2['port']);
    }

    /**
     * @depends testGetService
     */
    public function testRemoveService()
    {
        $client = $this->getClient();

        $res = $client->options('/v1/service/' . self::SERVICE_NAME);

        $this->assertEquals(200, $res->getStatusCode());
        $json = $this->getJSONFromResponse($res);

        $this->assertArrayHasKey('rows', $json);
        $this->assertCount(3, $json['rows']);

        $record = reset($json['rows']);

        $this->assertArrayHasKey('key', $record);

        $key = $record['key'];

        $res2 = $client->delete("/v1/service/$key");
        $json2 = $this->getJSONFromResponse($res2);

        $this->assertTrue(is_array($json2));

        $res3 = $client->options('/v1/service/' . self::SERVICE_NAME);
        $json3 = $this->getJSONFromResponse($res3);
        $this->assertArrayHasKey('rows', $json3);
        $this->assertCount(2, $json3['rows']);

        $json4 = false;

        try {
            $client->get("/v1/service/$key");
            $this->assertFalse(true); // should never happens, since the exception is expected
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = $e->getResponse();

            $this->assertEquals(404, $res->getStatusCode());

            $text = $res->getBody(true)->getContents();

            $json4 = json_decode($text, true);

            $this->assertTrue(is_array($json4));
            $this->assertArrayHasKey('success', $json4);
            $this->assertFalse($json4['success']);
        }
    }



    /**
     * @depends testRemoveService
     */
    public function testUpdateServiceStatus()
    {
        $client = $this->getClient();

        $res = $client->options('/v1/service/' . self::SERVICE_NAME);

        $this->assertEquals(200, $res->getStatusCode());
        $json = $this->getJSONFromResponse($res);

        $this->assertArrayHasKey('rows', $json);
        $this->assertCount(2, $json['rows']);

        $record = reset($json['rows']);

        $this->assertArrayHasKey('key', $record);

        $key = $record['key'];

        $res2 = $client->get("/v1/service/$key");
        $json2 = $this->getJSONFromResponse($res2);
        $this->assertTrue(is_array($json2));


        try {
            $res3 = $client->put("/v1/service/$key",[
                'json' => [
                    "service" => [
                        'status' => Service::STATUS_INACTIVE,
                        'filler' => 123,
                        "port"=>rand(0,65536)
                    ]
                ]
            ]);


        } catch (ClientException $e) {
            //echo "\n\n".$e->getResponse()->getBody()->getContents()."\n\n";
        }

        $json3 = $this->getJSONFromResponse($res3);
        $this->assertTrue(is_array($json3));
        $this->assertArrayHasKey('key', $json3);
        $this->assertEquals($record['key'], $json3['key']);

        $this->assertEquals(Service::STATUS_INACTIVE, $json3['result']['status']);
    }

    public function testRemoveInexistentService() {

        $client = $this->getClient();
        $key = hash('sha256', time());

        try {
            $res = $client->delete("/v1/service/$key",[
                'json' => [
                    'status' => Service::STATUS_INACTIVE,
                    'filler' => 123
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = $e->getResponse();

            $this->assertEquals(404, $res->getStatusCode());
        }

    }


    public function testUpdateInexistentService() {

        $client = $this->getClient();
        $key = hash('sha256', time());


        try {
            $res = $client->put("/v1/service/$key",[
                'json' => [
                    'service' => [
                        'status' => Service::STATUS_INACTIVE,
                        'filler' => 1234
                    ]
                ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = $e->getResponse();
            $this->assertEquals(404, $res->getStatusCode());
        }

    }



    public function testPingOperation() {
        $client = $this->getClient();
        $res = $client->get("/ping");
        $json = $this->getJSONFromResponse($res);
        $this->assertArrayHasKey('pong', $json);
        $this->assertEquals(1, $json['pong']);
    }

}
