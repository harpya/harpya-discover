<?php

namespace functional;

use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;

class BasicTest extends \PHPUnit\Framework\TestCase
{
    protected $config = ['base_uri' => 'http://webserver'];

    protected function getConfig($config = [])
    {
        return array_replace($this->config, $config);
    }

    public function testClearDB() {

        $user = getenv('MONGO_INITDB_ROOT_USERNAME');
        $pass = getenv('MONGO_INITDB_ROOT_PASSWORD');
        $host = getenv('MONGO_INITDB_HOST');

        $mongoHost = 'mongodb://' . $host;

        $manager = new \MongoDB\Driver\Manager($mongoHost, ['username' => $user,  'password' => $pass]);




        $bulk = new \MongoDB\Driver\BulkWrite();
        $bulk->delete([]);

        $manager->executeBulkWrite('discover.service', $bulk);
        $this->assertTrue(true);
    }


    /**
     * Make a call without any token, to get an error
     *
     * @depends testClearDB
     */
    public function testCallRestrictedServiceWithoutValidToken()
    {
        $client = new Client($this->getConfig());

        try {
            $res = $client->get('/auth');
            $text = $res->getBody()->getContents();
            echo $text;
        } catch (\Exception $e) {
            $lines = explode("\n", $e->getMessage());
            $lines[0] = '';
            $json = json_decode(join('', $lines), true);

            $this->assertArrayHasKey('code', $json);
            $this->assertEquals(1100, $json['code']);

            $this->assertEquals(400, $e->getCode());
        }
    }

    /**
     * Make a call to a public service, and returns the correct values
     */
    public function testDiscoverIsAlive()
    {
        $client = new Client($this->getConfig());

        $res = $client->get('/ping');
        $text = $res->getBody()->getContents();

        $json = json_decode($text, true);

        $this->assertEquals(200, $res->getStatusCode());
        $this->assertArrayHasKey('pong', $json);
    }

    /**
     * Try to execute a public service misconfigured (the action is wrong)
     */
    public function testCallPublicServiceActionsDoesNotExists()
    {
        $client = new Client($this->getConfig());

        $this->expectExceptionCode(404);
        $client->post('/ping');
    }

    public function testCallPrivateServiceWithInvalidToken()
    {
        $client = new Client($this->getConfig(['headers' => [
            'X-AUTH-API' => 'abc'
        ]]));

        $this->expectExceptionCode(403);
        $res = $client->get('/auth');
    }

    public function testCallPrivateServiceWithValidToken()
    {
        $client = new Client($this->getConfig([
            'headers' => [
                'X-AUTH-API' => getenv('HARPYA_AUTH_API'),
            ]
        ]));

        $res = $client->get('/auth');
        $text = $res->getBody()->getContents();

        $json = json_decode($text, true);

        $this->assertEquals(200, $res->getStatusCode());
        $this->assertArrayHasKey('ok', $json);
        $this->assertArrayHasKey('tm', $json);
    }


    public static $tag;

    public static function getTag() {
        if (!self::$tag) {
            self::$tag = time();
        }
        return self::$tag;
    }

    /**
     * Make a call to a public service, and returns the correct values
     */
    public function testCallRegisterService()
    {
        $client = new Client($this->getConfig());

        $res = $client->post('/v1/service', [
            'headers' => [
                'X-AUTH-API' => getenv('HARPYA_AUTH_API'),
            ],
            'json' => [
                'service' => [
                    'name' => 'myServiceTest'.self::getTag(),
                    'host' => 'https://service',
                    'port' => 123456,
                    'health_check_url' => '/check_status'
                ]
            ]
        ]);
        $text = $res->getBody()->getContents();

        $json = json_decode($text, true);


        $this->assertEquals(200, $res->getStatusCode());
        $this->assertTrue(is_array($json));
        $this->assertArrayHasKey('success', $json);
        $this->assertTrue($json['success']);
        $this->assertEquals('https://service:123456', $json['address']);
    }

    /**
     * Make a call to a public service, and returns the correct values
     */
    public function testGetServiceInfo()
    {
        $client = new Client($this->getConfig());

        $res = $client->request('options', '/v1/service/myServiceTest'.self::getTag(),
            ['headers' => [
                'X-AUTH-API' => getenv('HARPYA_AUTH_API'),
            ]
            ]);
        $text = $res->getBody()->getContents();

        $json = json_decode($text, true);

        $this->assertEquals(200, $res->getStatusCode());
        $this->assertTrue(is_array($json));
        $this->assertArrayHasKey('success', $json);
        $this->assertTrue($json['success']);
        $this->assertArrayHasKey('rows', $json);
    }

    /**
     * Make a call to a public service, and returns the correct values
     */
    public function testGetInexistentServiceInfo()
    {
        $client = new Client($this->getConfig());

        try {
            $client->request('options', '/service/inexistentService', ['headers' => [
                'X-AUTH-API' => getenv('HARPYA_AUTH_API'),
            ]]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = $e->getResponse();

            $this->assertEquals(404, $res->getStatusCode());

            $text = $res->getBody(true)->getContents();

            $json = json_decode($text, true);

            $this->assertTrue(is_array($json));
            $this->assertArrayHasKey('success', $json);
            $this->assertFalse($json['success']);
        }
    }
}
