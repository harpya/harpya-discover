<?php

include_once __DIR__ . '/../vendor/autoload.php';
include_once __DIR__ . '/../bootstrap.php';

use \ParagonIE\Halite\Symmetric\Crypto;

$obj = new \harpya\discover\AuthToken();

try {
    $obj->loadKey(getenv('KEY_PATH'));

    $initial = bin2hex(random_bytes(10));
    $initial .= $initial;

    $encrypted = Crypto::encrypt(
        new \ParagonIE\Halite\HiddenString($initial),
        $obj->getKey(),
        \ParagonIE\Halite\Halite::ENCODE_HEX
    );

    echo 'Token = ' . $encrypted;
} catch (Exception $e) {
    echo "\nError: " . $e->getMessage();
}

echo "\n";
