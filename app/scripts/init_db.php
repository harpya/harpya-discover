<?php

include_once __DIR__ . '/../vendor/autoload.php';

use MongoDB\Driver\Command;

$user = getenv('MONGO_INITDB_ROOT_USERNAME');
$pass = getenv('MONGO_INITDB_ROOT_PASSWORD');
$host = getenv('MONGO_INITDB_HOST');

$mongoHost = 'mongodb://' . $host;

$manager = new \MongoDB\Driver\Manager($mongoHost, ['username' => $user,  'password' => $pass]);

$bulk = new \MongoDB\Driver\BulkWrite();
$bulk->delete([]);

$manager->executeBulkWrite('discover.service', $bulk);

$manager->executeCommand('discover', new Command(
    ['createIndexes' => 'service',
        'indexes' => [[
            'name' => 'unique_service',
            'key' => ['name' => 1, 'host' => 1, 'port' => 1],
            'ns' => 'discover.service',
            'unique' => true
        ]]]
));

echo "\n Index created \n";
