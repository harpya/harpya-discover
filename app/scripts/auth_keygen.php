<?php

include_once __DIR__ . '/../vendor/autoload.php';
include_once __DIR__ . '/../bootstrap.php';

$obj = new \harpya\discover\AuthToken();

try {
    $obj->saveKey(getenv('KEY_PATH'));
    echo 'New key generated at ' . getenv('KEY_PATH');
} catch (Exception $e) {
    echo "\nError: " . $e->getMessage();
}

echo "\n";
