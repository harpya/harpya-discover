<?php

use Phinx\Migration\AbstractMigration;

class CreateInitialTables extends AbstractMigration
{
    public function change()
    {
        $this->table('service')
            ->addColumn('key', 'string', ['length' => 80])
            ->addColumn('status', 'integer', ['default' => 1])
            ->addColumn('port', 'integer', ['default' => 80])

            ->addColumn('name', 'string', ['length' => 64])
            ->addColumn('version', 'string', ['length' => 10])
            ->addColumn('host', 'string', ['length' => 100])
            ->addColumn('health_check_url', 'string', ['length' => 120])

            ->addTimestamps()
            ->addIndex(['name', 'host', 'port'], ['unique' => true])
            ->addIndex('key')
            ->addIndex(['name', 'version'])
            ->save();

    }
}
