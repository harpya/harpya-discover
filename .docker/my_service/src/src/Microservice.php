<?php

namespace harpya\sample;

class Microservice
{
    protected $env;
    protected $raw;

    public function __construct($env)
    {
        $this->env = $env;
        $this->raw = file_get_contents('php://stdin');
    }

    public function exec()
    {
        switch ($_SERVER['REQUEST_URI']) {
            case '/ping':
                $return = [
                    'command' => 'pong'
                ];
                break;
            default:
                $return = $this->dumpAllData();
                break;
        }

        $this->logRequest($return);

        $this->sendResponse($return);
    }

    protected function dumpAllData()
    {
        $return = [
            'time' => time(),
            'method' => $_SERVER['REQUEST_METHOD'],
            'uri' => $_SERVER['REQUEST_URI'],
            'data' => [
                'post' => $_POST,
                'raw' => $this->raw,
            ],
        ];

        return $return;
    }

    protected function sendResponse($response)
    {
        header('Content-type: application/json');
        echo json_encode($response, true) . "\n";
        exit;
    }

    protected function logRequest($data)
    {
        $data['origin'] = $_SERVER['REMOTE_ADDR'] . ':' . $_SERVER['REMOTE_PORT'];

        $s = "\n" . date('Y-m-d H:i:s') . ' ' . print_r($data, true);

        error_log($s, 3, '/tmp/requests.log');
    }
}
