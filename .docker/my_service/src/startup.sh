#!/usr/bin/env bash

if [ -d "/srv/app/vendor" ]; then
//
else
    cd /srv/app
    composer install
fi

# make a call to register on Discover...
php /srv/app/bootstrap.php

# change to resolve only the index.php
php -S "0.0.0.0:${MICRO_SERVICE_SAMPLE_PORT}" -t /srv/app/public
