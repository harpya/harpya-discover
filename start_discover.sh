#!/usr/bin/env bash

docker-compose up -d
docker-compose exec php composer update
docker-compose exec php ./vendor/bin/phinx migrate
